# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
FROM tomcat:8.5.41-jre8-alpine

RUN rm -rf /usr/local/tomcat/webapps/*

COPY ./target/GamingNow-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ROOT.war

CMD ["catalina.sh", "run"]

EXPOSE 8080
