# GamingNow! Server #

Servidor desarrollado usando las librerías de Spring Boot

## Docker ##

Se ha preparado una imagen de Docker para ejecutar un cotenedor con el servidor, compilado en varias arquitecturas:

- arm64
- armv7
- amd64

También se ha subido a un repositorio público en Docker HUB (https://hub.docker.com/r/raulrm00/gaming-now-server)

### Ejecutar servidor desde imagen descargada ###

Para ejecutar el servidor fácilmente podemos descargar la imagen desde Docker HUB y ejecutar un contenedor:

```
docker run -d -p 80:8080 raulrm00/gaming-now-server:alpine-0.0.1
```

### Compilar imagen Docker ###

```
docker build .
```

## Maven ##

Si tenemos maven instalado, el repositorio viene configurado con varios *goals* para la ejecución local del servidor

```
mvn spring-boot:run
```
