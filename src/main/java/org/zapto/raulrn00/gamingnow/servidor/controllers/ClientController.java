/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zapto.raulrn00.gamingnow.servidor.controllers;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.zapto.raulrn00.gamingnow.servidor.models.Client;
import org.zapto.raulrn00.gamingnow.servidor.models.Room;
import org.zapto.raulrn00.gamingnow.servidor.repositories.ClientRepository;

/**
 *
 * @author raul
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/api/clients")
public class ClientController {

    private final ClientRepository clientRepository;

    public ClientController(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @GetMapping
    public List<Client> list() {
        return clientRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Client> get(@PathVariable UUID id) {
        Optional<Client> optional = clientRepository.findById(id);
        if (optional.isPresent()) {
            return ResponseEntity.ok().body(optional.get());
        }
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}/rooms")
    public List<Room> getRooms(@PathVariable UUID id) {
        Optional<Client> optional = clientRepository.findById(id);
        if (optional.isPresent()) {
            return new ArrayList<>(optional.get().getRooms());
        }
        return new ArrayList<>();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Client> put(@PathVariable UUID id, @RequestBody Client input) {
        Optional<Client> optional = clientRepository.findById(id);
        if (optional.isPresent()) {
            Client client = optional.get();
            client.setName(input.getName());
            client.setEmail(input.getEmail());
            return ResponseEntity.ok().body(clientRepository.save(client));
        }
        return ResponseEntity.noContent().build();
    }

    @PostMapping
    public ResponseEntity<Client> post(@RequestBody Client input) {
        return ResponseEntity.ok().body(clientRepository.save(input));
    }

    @PostMapping("/login")
    public ResponseEntity<Client> login(@RequestBody Client input) {
        Optional<Client> optional = clientRepository.findOneByEmail(input.getEmail());
        if (optional.isPresent()) {
            Client client = optional.get();
            if (client.getPassword().equals(input.getPassword())) {
                return ResponseEntity.ok().body(client);
            }
        }
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Client> delete(@PathVariable UUID id) {
        Optional<Client> optional = clientRepository.findById(id);
        if (optional.isPresent()) {
            Client client = optional.get();
            clientRepository.delete(client);
        }
        return ResponseEntity.noContent().build();
    }

}
