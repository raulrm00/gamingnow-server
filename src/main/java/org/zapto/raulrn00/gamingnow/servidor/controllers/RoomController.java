/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zapto.raulrn00.gamingnow.servidor.controllers;

import java.util.ArrayList;
import java.util.HashSet;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.zapto.raulrn00.gamingnow.servidor.models.Client;
import org.zapto.raulrn00.gamingnow.servidor.models.Room;
import org.zapto.raulrn00.gamingnow.servidor.repositories.ClientRepository;
import org.zapto.raulrn00.gamingnow.servidor.repositories.RoomRepository;

/**
 *
 * @author raulr
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/api/rooms")
public class RoomController {

    private final RoomRepository roomRepository;
    private final ClientRepository clientRepository;

    public RoomController(
            RoomRepository roomRepository,
            ClientRepository clientRepository
    ) {
        this.roomRepository = roomRepository;
        this.clientRepository = clientRepository;
    }

    @GetMapping()
    public List<Room> list() {
        return roomRepository.findAll();
    }

    @GetMapping("/{id}")
    public Room get(@PathVariable UUID id) {
        Optional<Room> optional = roomRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        }
        return null;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id) {
        roomRepository.deleteById(id);
    }

    @PostMapping("/join/{game}")
    public Room join(@RequestBody Client clientParam, @PathVariable String game) {
        Optional<Client> optionalClient = clientRepository.findById(clientParam.getId());
        if (optionalClient.isPresent()) {
            Client client = optionalClient.get();
            Optional<Room> optionalRoom = roomRepository.findOneByGame(game);
            if (optionalRoom.isPresent()) {
                Room room = optionalRoom.get();
                if (room.isValid()) {
                    room.setClients_number(room.getClients_number() + 1);
                    room.getClients().add(client);
                    return roomRepository.save(room);
                }
                room.setClients(new HashSet<>());
                room.setValid(true);
                room.setClients_number(1);
                room.getClients().add(client);
                return roomRepository.save(room);
            }
            Room room = new Room();
            room.setGame(game);
            room.setValid(true);
            room.setClients_number(1);
            room.getClients().add(client);
            return roomRepository.save(room);
        }
        return null;
    }

    @GetMapping("/{game}/clients")
    public List<Client> listClientsFromRoom(@PathVariable String game) {
        Optional<Room> optional = roomRepository.findOneByGame(game);
        if (optional.isPresent()) {
            List<Client> clients = new ArrayList<>(optional.get().getClients());
            return clients;
        }
        return new ArrayList<>();
    }

}
