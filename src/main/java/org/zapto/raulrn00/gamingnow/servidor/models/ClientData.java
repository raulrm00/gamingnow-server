/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zapto.raulrn00.gamingnow.servidor.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;
import org.zapto.raulrn00.gamingnow.servidor.models.enumerations.Platform;

/**
 *
 * @author raul
 */
@Getter
@Setter
@Entity
public class ClientData implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @DateTimeFormat
    private Date created_at;

    private String username;

    @Enumerated(EnumType.STRING)
    private Platform platform;

    @ManyToOne
    @JsonIgnoreProperties(value = "data", allowSetters = true)
    private Client client;

    @PrePersist
    protected void prePersist() {
        created_at = new Date();
    }

}
