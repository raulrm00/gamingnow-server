/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zapto.raulrn00.gamingnow.servidor.repositories;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.zapto.raulrn00.gamingnow.servidor.models.ClientData;

/**
 *
 * @author raul
 */
public interface ClientDataRepository extends JpaRepository<ClientData, UUID> {
}
