/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zapto.raulrn00.gamingnow.servidor.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.Transient;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author raulr
 */
@Getter
@Setter
@Entity
public class Room implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @DateTimeFormat
    private Date created_at;

    @DateTimeFormat
    private Date last_update;

    /**
     * Unique ID (slug) that identifies the game
     */
    @Column(name = "game")
    private String game;

    @Transient
    private boolean valid;

    @Transient
    private long clients_number;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JsonIgnore
    private Set<Client> clients = new HashSet<>();

    @PrePersist
    protected void prePersist() {
        created_at = new Date();
        last_update = new Date();
    }

    @PreUpdate
    protected void preUpdate() {
        last_update = new Date();
    }

    @PostLoad
    protected void postLoad() {
        valid = System.currentTimeMillis() - last_update.getTime() < 3600000;
        clients_number = clients.size();
    }

}
