package org.zapto.raulrn00.gamingnow.servidor;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.zapto.raulrn00.gamingnow.servidor.models.Client;
import org.zapto.raulrn00.gamingnow.servidor.models.ClientData;
import org.zapto.raulrn00.gamingnow.servidor.models.enumerations.Platform;
import org.zapto.raulrn00.gamingnow.servidor.repositories.ClientDataRepository;
import org.zapto.raulrn00.gamingnow.servidor.repositories.ClientRepository;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner demo(ClientRepository clientRepository, ClientDataRepository clientDataRepository) {
        return (args) -> {
            // ----- Clients -----
            Client client1 = new Client();
            client1.setName("Raúl");
            client1.setEmail("raulrm00@gamingnow.zapto.org");
            client1.setPassword("123456");
            client1 = clientRepository.save(client1);
            ClientData clientData1 = new ClientData();
            clientData1.setPlatform(Platform.STEAM);
            clientData1.setUsername("raulrm00");
            clientData1.setClient(client1);
            ClientData clientData2 = new ClientData();
            clientData2.setPlatform(Platform.EPIC_GAMES);
            clientData2.setUsername("raulrm00");
            clientData2.setClient(client1);
            clientDataRepository.save(clientData1);
            clientDataRepository.save(clientData2);
            Client client2 = new Client();
            client2.setName("Pepe");
            client2.setEmail("pepe@gamingnow.zapto.org");
            client2.setPassword("123456");
            client2 = clientRepository.save(client2);
            ClientData clientData3 = new ClientData();
            clientData3.setPlatform(Platform.PLAY_STATION_NETWORK);
            clientData3.setUsername("PEPES4");
            clientData3.setClient(client2);
            ClientData clientData4 = new ClientData();
            clientData4.setPlatform(Platform.XBOX_LIVE);
            clientData4.setUsername("PEPEXBOX");
            clientData4.setClient(client2);
            ClientData clientData5 = new ClientData();
            clientData5.setPlatform(Platform.STEAM);
            clientData5.setUsername("PEPESTEAM");
            clientData5.setClient(client2);
            clientDataRepository.save(clientData3);
            clientDataRepository.save(clientData4);
            clientDataRepository.save(clientData5);
            Client client3 = new Client();
            client3.setName("Jose");
            client3.setEmail("jose@gamingnow.zapto.org");
            client3.setPassword("123456");
            client3 = clientRepository.save(client3);
            ClientData clientData6 = new ClientData();
            clientData6.setPlatform(Platform.STEAM);
            clientData6.setUsername("JOSESTEAM");
            clientData6.setClient(client3);
            clientDataRepository.save(clientData6);
        };
    }

}
