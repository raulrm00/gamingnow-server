/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zapto.raulrn00.gamingnow.servidor.models.enumerations;

/**
 *
 * @author raul
 */
public enum Platform {
    STEAM,
    EPIC_GAMES,
    PLAY_STATION_NETWORK,
    XBOX_LIVE
}
