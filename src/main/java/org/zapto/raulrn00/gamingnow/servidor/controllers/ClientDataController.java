/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zapto.raulrn00.gamingnow.servidor.controllers;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.zapto.raulrn00.gamingnow.servidor.models.ClientData;
import org.zapto.raulrn00.gamingnow.servidor.repositories.ClientDataRepository;

/**
 *
 * @author raul
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/api/client-data")
public class ClientDataController {

    private final ClientDataRepository clientDataRepository;

    public ClientDataController(ClientDataRepository clientDataRepository) {
        this.clientDataRepository = clientDataRepository;
    }

    @GetMapping()
    public List<ClientData> list() {
        return clientDataRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClientData> get(@PathVariable UUID id) {
        Optional<ClientData> optional = clientDataRepository.findById(id);
        if (optional.isPresent()) {
            return ResponseEntity.ok().body(optional.get());
        }
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<ClientData> put(@PathVariable UUID id, @RequestBody ClientData input) {
        Optional<ClientData> optional = clientDataRepository.findById(id);
        if (optional.isPresent()) {
            ClientData data = optional.get();
            data.setPlatform(input.getPlatform());
            data.setUsername(input.getUsername());
            return ResponseEntity.ok().body(clientDataRepository.save(data));
        }
        return ResponseEntity.noContent().build();
    }

    @PostMapping
    public ClientData post(@RequestBody ClientData input) {
        return clientDataRepository.save(input);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ClientData> delete(@PathVariable UUID id) {
        Optional<ClientData> optional = clientDataRepository.findById(id);
        if (optional.isPresent()) {
            ClientData data = optional.get();
            clientDataRepository.delete(data);
        }
        return ResponseEntity.noContent().build();
    }

}
